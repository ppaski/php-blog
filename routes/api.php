

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');
Route::get('posts', 'API\PostsController@getAll');
Route::get('posts/{id}', 'API\PostsController@get');

Route::middleware('auth:api')->group(function () {
    Route::post('posts', 'API\PostsController@create');
    Route::post('posts/{id}', 'API\PostsController@update');
    Route::get('users', 'API\UsersController@getAll');
    Route::delete('users/{id}', 'API\UsersController@delete');
    Route::delete('posts/{id}', 'API\PostsController@delete');
});
