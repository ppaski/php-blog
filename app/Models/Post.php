<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'title', 'content', 'author_id', 'created_on', 'image_id'
    ];
}
