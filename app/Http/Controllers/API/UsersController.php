<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Post;
use App\User;

class UsersController extends BaseController
{
    public function getAll()
    {
        $users = User::all();
        return $this->sendResponse($users->toArray(), 'Users retrieved successfully.');
    }

    public function delete($id)
    {
        $user = User::find($id);

        $user->delete();
    }
}
