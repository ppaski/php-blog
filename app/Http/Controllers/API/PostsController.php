<?php


namespace App\Http\Controllers\API;
use App\Models\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Post;
use Illuminate\Support\Facades\DB;
use Validator;

class PostsController extends BaseController
{
    public function get($id)
    {
        $post = DB::table('posts')
            ->where('posts.id', '=', $id)
            ->join('users', 'posts.author_id', '=', 'users.id')
            ->join('images', 'posts.image_id', '=', 'images.id')
            ->select('posts.id', 'posts.title', 'posts.content', 'posts.created_on',
                'users.name as author', 'images.image')
            ->get();

        return $this->sendResponse($post, 'Post retrieved successfully.');
    }

    public function update($id, Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'title' => 'required',
            'content' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $date = date('Y-m-d H:i:s');
        $post = DB::table('posts')
            ->where('posts.id',  $id)
            ->update(['title' => $input['title'],
                'content' => $input['content'],
                'created_on' => $date]);

        return $this->sendResponse($post, 'Post updated successfully.');
    }

    public function delete($id)
    {
        $post = Post::find($id);

        $post->delete();
    }

    public function getAll()
    {
        $posts = DB::table('posts')
            ->join('users', 'posts.author_id', '=', 'users.id')
            ->join('images', 'posts.image_id', '=', 'images.id')
            ->select('posts.id', 'posts.title', 'posts.content', 'posts.created_on',
                'users.name as author', 'images.image')
            ->get();
        return $this->sendResponse($posts->toArray(), 'Posts retrieved successfully.');
    }

    public function create(Request $request)
    {
        //$image = new Image();
        $image['image'] = $request['image'];
        $createdImage = Image::create($image);

        //$input = new Post();
        $input['title'] = $request['title'];
        $input['content'] = $request['content'];
        $input['image_id'] = $createdImage->id;

        $validator = Validator::make($input, [
            'title' => 'required',
            'content' => 'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = auth()->user();
        $date = date('Y-m-d H:i:s');

        $input['author_id'] = $user->id;
        $input['created_on'] = $date;

        $post = Post::create($input);

        return $this->sendResponse($post->toArray(), 'Post created successfully.');

    }
}
